import pandas as pd
import numpy as np
import math

#https://www.kaggle.com/caesarlupum/ashrae-start-here-a-gentle-introduction
def reduce_mem_usage(df, verbose=True):
    numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    start_mem = df.memory_usage().sum() / 1024**2    
    for col in df.columns:
        col_type = df[col].dtypes
        if col_type in numerics:
            c_min = df[col].min()
            c_max = df[col].max()
            if str(col_type)[:3] == 'int':
                if c_min > np.iinfo(np.int8).min and c_max < np.iinfo(np.int8).max:
                    df[col] = df[col].astype(np.int8)
                elif c_min > np.iinfo(np.int16).min and c_max < np.iinfo(np.int16).max:
                    df[col] = df[col].astype(np.int16)
                elif c_min > np.iinfo(np.int32).min and c_max < np.iinfo(np.int32).max:
                    df[col] = df[col].astype(np.int32)
                elif c_min > np.iinfo(np.int64).min and c_max < np.iinfo(np.int64).max:
                    df[col] = df[col].astype(np.int64)  
            else:
                if c_min > np.finfo(np.float16).min and c_max < np.finfo(np.float16).max:
                    df[col] = df[col].astype(np.float16)
                elif c_min > np.finfo(np.float32).min and c_max < np.finfo(np.float32).max:
                    df[col] = df[col].astype(np.float32)
                else:
                    df[col] = df[col].astype(np.float64)    
    end_mem = df.memory_usage().sum() / 1024**2
    if verbose: print('Mem. usage decreased to {:5.2f} Mb ({:.1f}% reduction)'.format(end_mem, 100 * (start_mem - end_mem) / start_mem))
    return df

def convert_timestamp(df, weather_df, building_meta_df):
    df['timestamp'] = pd.to_datetime(df['timestamp'])
    weather_df['timestamp'] = pd.to_datetime(weather_df['timestamp'])
    building_meta_df['primary_use'] = building_meta_df['primary_use'].astype('category')

def convert_strings_to_category(df, building_meta_df):
    temp_df = df[['building_id']]
    temp_df = temp_df.merge(building_meta_df, on=['building_id'], how='left')
    del temp_df['building_id']
    df = pd.concat([df, temp_df], axis=1)
    return df

def split_timestamp(df):
    #df['timestamp_int'] = pd.Series(df['timestamp'], dtype=int)
    df['timestamp'] = pd.to_datetime(df['timestamp'])

    df['month_datetime'] = df['timestamp'].dt.month.astype(np.int8)
    df['weekofyear_datetime'] = df['timestamp'].dt.weekofyear.astype(np.int8)
    df['dayofyear_datetime'] = df['timestamp'].dt.dayofyear.astype(np.int16)

    df['hour_datetime'] = df['timestamp'].dt.hour.astype(np.int8)  
    df['day_week'] = df['timestamp'].dt.dayofweek.astype(np.int8)
    df['day_month_datetime'] = df['timestamp'].dt.day.astype(np.int8)
    df['week_month_datetime'] = df['timestamp'].dt.day/7
    df['week_month_datetime'] = df['week_month_datetime'].apply(lambda x: math.ceil(x)).astype(np.int8)

    #df['timestamp_int'] = df['dayofyear_datetime']
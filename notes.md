10/21/19
3 relevant tables for training
table building_metadata.csv does not change test/train
features in weather_train.csv and train.csv change to corresponding test files

train.csv building_id key maps to building_id in build_metadata.csv 
building_metadata.csv site_id key maps to site_id in weather_train.csv
train.csv timestamp key maps to timestamp in weather_train.csv

2 separate models that link to one another? one passes regression value to another?

10/22/19

methods to feature engineer - assuming buildings are in the US lat/lon range?
quantitative/sequential values: max, min, sum, std, abs_max, abs_min, first_x, last_x
try different combinations for series with convertible units?
building surface area on a windy day -> dew/air temp? - floors, sqft, winddirection/windspeed
area_of_building_side ~= sqrt(sqft)*floors
estimated surface area (Area of hot object) = area_of_building_side*4 + sqft (assuming building is a square prism lmao)
pow 4 absolute temperature differential = kelvins(dewtemp)**4 - kelvins(airtemp)**4
estimated emissivity coefficient = fn(building_id, site_id, year_built)?-also impacted by specific heat/emissivity of surrounding material-> seasons?
day of year important due to work scheduling -> day of week classification? find calendar library that does datetime give day of week?
hour of day- work hours.. what about daylight savings? holidays? hopefully utc.. look up hours of daylight from external gov site?- assume buildings on similar lat/lon
http://www.egr.unlv.edu/~eebag/Sunlight%20and%20its%20Properties.pdf
http://hyperphysics.phy-astr.gsu.edu/hbase/thermo/stefan.html
Declination angle? = radians(23.45)*sin(360(nday-81)/365)
distance to sun = 150{1+0.017*sin(360*(nday-93)/365)} (units.. Mkm)
radiation intensity ratio = (radius_of_sun/distance_to_sun)**2 (radius of sun = 6.955 x 10**8km)
height coefficient = fn(floors, building_id, site_id)
estimated angle from sun  = fn(hour_of_day) 6am-6pm -(90-lat) to 0 to (90-lat)? no earth spins on axis ~ 20 smth degrees
consider sin or cos of declination angle? * area_of_building_side or sqft
shadow_length = building_height*tan(estimated_angle_)
air mass = sqrt(1-(shadow_length/building_height)**2)??
https://en.wikipedia.org/wiki/Heat_transfer
advection?? = windspeed*pressure

10/23/19

todo: port over more fn into gentle-intro-utility
airmass? meh
mean meter_reading grouped by timestamp?
think of pairs 1 column max, min, sum, std, abs_max, abs_min, first_x, last_x other column groupby
combinations of diffs?
handle missing values see gentle intro

compare nn performance vs. lgbm
-todo nn need to normalize and one-hot encode vars most likely... make sure upper and lower bounds of test set features match those of train set features

10/24/19
refactor ashrae_feature_engineering.py into functions?-done?-add more engineered feats later + analyze and drop correlated dim reduction
fix df_fillna
build and compare models lgb and nn?- see 10/23 nn todo

left merge building_meta_df and weather_train_df to train_df?- probably takes up alot more mem..
-is time spent on gridsearch even worth?

10/25/19
confusion matrix.. but thats for classification?
Drop features with mode frequency > 98.5% of data? no one hot encoded..
drop low variance features?

10/30/19
fix memory issues during feature_engineering by streaming dataframe chunks during merge process
save computation time by saving engineered features/data to file
apply similar methods when implementing models..

add conditional to delete/overwrite or skip computation per feature_engineered_* function

os.remove usage correct? due to pd.merge..

use pd.HDFStore in models
see https://realpython.com/fast-flexible-pandas/

fix feature_engineered_df not skipping computation with new_features=False

should save to source control?
need to run with changes to header

10/31/19
ValueError: You are trying to merge on datetime64[ns] and object columns. If you wish to proceed you should use pd.concat

timestamp is datetime64? use int instead? name timestamp_int.. what about the two datetime64 timestamp columns? what about save_mem_usage types?

site_id.. object columns? set 
?don't need to do streaming with 16gb of ram? why did MemoryError popup before-loaded csv in vsc?
if read_csv is used, need to convert datetime64 from object using to_datetime


Removing weired data on site_id 0
As you can see above, this data looks weired until May 20. It is reported in this discussion by @barnwellguy that All electricity meter is 0 until May 20 for site_id == 0. Let's remove these data from training data.

11/1/19
consider adding/dropping more features later?- also analyze and deal with number of missing values.. better off using interpolation than blindly setting to -999
run lgbm on smaller subset of data that fits in memory
look at feature importance and make decision to drop features..
run lstm nn on all of dataset..?
get some low low rmse values heh
log1p meter_reading?

what layer of tf to use? .. estimator ofc
-need to one-hot encode for 'primary use', 'meter'.. when using nn or any model not gbm

11/2/19
get rmse lower than 1 for lgbm.. focus on dropping features today or larger chunksize
for nn: one-hot encode, normalize
do some plotting to find strange anomalies in data not suitable for creating accurate models?

11/4/19
time to drop high correlated/extraneous:
sa_building_side
total_sa_building_side
est_sa_building
declination_angle
distance_to_sun
radiation_intensity_ratio
absolute_temperature_diff_pow4
advection_coef

possible reasons why meter_reading is screwed in merged_data_train:
plz use np.log1p on meter_reading? 

need to add other common features- but how to account for padding when doing rolling aggregation functions?-don't just set flat values over what you've used groupby over.. likely day

11/5/19
waow rather low rmse values!??
need to run model on even larger subset of data- might overfit for subset or smth
idk how to run a lgbm model with a generator (don't think that would even work with gbms with threshold plitting data with node threshold trees) 
first try different cv, and on all of dataset

good candidates for aggregate groupby fns: #20
 'declination_angle' 'distance_to_sun'
 'radiation_intensity_ratio' 
 'square_feet' 'year_built' 'floor_count' 'sa_building_side'
 'total_sa_building_side' 'est_sa_building' 'age' 
 'air_temperature' 'cloud_coverage' 'dew_temperature' 'precip_depth_1_hr'
 'sea_level_pressure' 'wind_direction' 'wind_speed'
 'absolute_temperature_diff_pow4' 'advection_coef' 'offset'

good ranges to groupby over: #13
'building_id' 'site_id' 'meter' 'month_datetime' 'weekofyear_datetime'
 'dayofyear_datetime' 'hour_datetime' 'day_week' 'day_month_datetime'
 'week_month_datetime' 'primary_use' 'age' 'offset'

good aggregation fns (also across different x ranges..? need to find min size per groupby): #6 *x?
max, min, sum, std, abs_max, abs_min 
.. across first_x, last_x

given the cities notebooks:
https://www.kaggle.com/selfishgene/historical-hourly-weather-data/download
https://www.kaggle.com/patrick0302/locate-cities-according-weather-temperature

B = 360/364*(day_of_year - 81)
E = 9.87*sin(2*B) - 7.53*cos(B) - 1.5*sin(B)
SolarTime = CivilTime + E #(or minus?)

hour_angle = omega = radians(15)*(12-SolarTime) 
declination_angle = delta
latitude_angle = phi

sun_altitude_angle = alpha = arccos(sin(declination_angle)*sin(latitude_angle) + cos(declination_angle)*cos(latitude_angle)*cos(hour_angle))

sun_azimuth_angle = arcsin(cos(declination_angle)*sin(hour_angle)/cos(sun_altitude_angle))
or
sun_azimuth_angle = arccos((sin(sun_altitude_angle)*sin(latitude_angle)-sin(declination_angle))/(cos(sun_altitude_angle)*cos(latitude_angle)))

need to test if altitude angle is > 90 and pick one with lambda
calculate sun altitazimuth angles? per site_id

cloud_coverage = np.normalize() #0-1

surface_radiation_ratio = cloud_coverage*radiation_intensity_ratio*max(sin(azimuth_angle), 0)

try just 1-2: aggregate over day.. absolute_temperature_diff_pow4, advection_coef
aggregate over month? mean,min,max for radiation intensity ratio????? what about some 



todo:
fix TypeError on datetime64+timedelta problem for column "solar_time
try lstm nn with generator on entirety of dataset
see above do 3-6 agg fns

11/6/19
todo:
solve NullFrequencyError:
https://pandas.pydata.org/pandas-docs/stable/reference/offset_frequency.html#frequencies
?-didn't read copypasta
read more docs?
is doing latitude even worth it.. for azimuth angle?

11/8/19
why does the first parallelize dataframe merge fn fail?
somehow cannot process datetime64 objects in process..?
why are there datetime64 objects? shouldn't everything be a string, int, or float?- after read_csv w/ chunksize + other params..
cannot pickle??..
only df and weather_df could have datetime64..
maybe use Process instead of pool.map..?
what objects are local..
share state between processes for joinee_df..? nah its fine? unless MemoryError
break dataframe into series into arrays pass as multiprocessing.Array

when doing multiprocessing, why does parallelize_dataframe_merge get skipped over
stage 2 runs?-fukit

try locking for each 3 stages.. after getting non multiprocessing version to work... won't adding features and debugging be more difficult with multiprocessing implemented?
need to rewrite merge_with_reader params..? zip args
any valid usage of Queue to facilitate shared data between processes, namely weather_df and building_metadata_df? just use threads since threads have shared memory lulw..

problems with certain files?
-merging causes portion of values to not be written, instead blanks.. swap parameter inputs for pd.merge..
think function is optimized on the smaller df being the second input.. or smth
see merge_with_reader


start modeling instead of doing an excess of feature engineering?
-several features appear
more printing/plotting?
try with new features with lgbm.. see if np.log1p rmse values get lower than ~ .55? - optimism

edit and write up lstm model
conda install keras, tf etc.

deal with possible NaN values in sun_azimuth_angle
also problem- due to same day of year, many features may be highly correlated/redundant?
-still would be nice to parallize merge fn, could save alot of time..
cannot simply dropna for test data/submission... else no prediction for that row/set of features
if test.. use df.rename({'index':'row_id'}) df.reset_index()? look at Unamed 0: xyz after merge?

11/9/19

implement multiprocessing at some point...

nah just create models and check rmse
timestamp align for solar time?

simplify..
split into 4 models 1 per meter-do this preprocessing step in feat_eng?
-have 4 separate tables per meter reading... or however the optuna notebook does it?
do lags.. aka max, min, mean, std rolling per x in feat_eng why 3, 72?
-should lags be done per meter?.. 
optuna for lstm?
-read notebook figure out merge meter predict over 4 models?-if rows are indexed.. ez?
try submitting sometime lulw

11/15/19
need to explore test.csv or feature_engineered_test.csv isna().sum()
bfill for missing site_id values? why are there missing site_id values if isna().sum() was 0 for train.csv()? something wrong with site_city_mappings...? do a unique().count() for site_id, should eq 16?
-do this after checking merged test for dimensionality discrepancies

just buy more ram, buy a ssd, get better internet...
l2 cloud comp

lack of city, sun_azimuth angle columns due to it simply not being implemented b4 :/?
-probably doesn't matter, low feature importance probably

11/19/2019
consider using helper fns to encap type conversion and to_feather + simple edit in gentle_intro instead

11/23/19
phuckit memory too smoll-use streaming merge + lstm?
todo:
1 runthough to make sure everything still works-read
add lags-jk don't, save mem?
implement lstm
train lgbm over largest subset that will fit in memory? compare mse
current mse/rmse too low? overfit on subset?-figure out a proper cv?
submission.csv

add leak meme- alot of web scraping for uni websites.. ucb, ucl, ucf, cornell, asu

timesplitting train/validation sets?-nah random sampling/splitting fine

https://www.kaggle.com/yamsam/ashrae-highway-kernel-route2
https://www.kaggle.com/yamsam/ashrae-leak-validation-and-more

just focus on lstm?
add leaked/webscrapped features later...
need to shuffle/randomize sampling train/eval over time

12/4/19
+ 6consider using streamed chunks.. generator.. if 

12/7/19
rewrite merge process without parallelization, but with generators.. (see if that takes up less memory)
also need to rewrite interpolate_by_mean.. in doing so.. or look at other notebooks for bfill or ffill

12/8/19
check index=False?

12/9/19
reading speed with variable len(usecols) same for all len?- only memory consumption changes?
could just use feather meme if you cut some features..

12/10/19
weighting of copypasta notebook meme:

https://www.kaggle.com/khoongweihao/ashrae-leak-validation-bruteforce-heuristic-search

sample_submission1 = pd.read_csv('../input/ashrae-kfold-lightgbm-without-leak-1-08/submission.csv', index_col=0)
sample_submission2 = pd.read_csv('../input/ashrae-half-and-half/submission.csv', index_col=0)
sample_submission3 = pd.read_csv('../input/ashrae-highway-kernel-route4/submission.csv', index_col=0)

12/11/19
its not plagarism.. its a hybridization of published methodologies...
try bruteforce heuristic search on nfl yards scored meme?

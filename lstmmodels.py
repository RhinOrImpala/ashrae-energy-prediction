import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
#import optuna

import os

from ashrae_feature_engineering import write_merged_dataframes, read_merged_dataframes
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
import pickle


from sklearn.preprocessing import MinMaxScaler


import numpy as np
import time
import pandas as pd

from absl import app
from absl import flags
import absl.logging as _logging  

import tensorflow as tf
#from lgb_gen import generator, extract_lgbX, test_seg_generator
from ashrae_feature_engineering import read_merged_dataframes

import shutil

FLAGS = flags.FLAGS

flags.DEFINE_integer('train_size',
    default=100,
    help="Number of 50000 length segments to train on. Remainder will be for validation."
)
flags.DEFINE_integer('cellsize', 
    default=16, 
    help='Size of the cells usually RNN.'
)
flags.DEFINE_integer('n_layers', 
    default=3, 
    help='Number of stacked RNN cells '
    '(needed for tensor shapes but code must be changed manually).'
)
flags.DEFINE_float('dropout_pkeep', 
    default=0.7, 
    help='Probability of neurons not being dropped (should be between 0.5 and 1).'
)
flags.DEFINE_integer('nb_epochs', 
    default=100, 
    help='Number of times the model sees all the data during training.'
)
flags.DEFINE_string('outdir',
    default="savedmodel",
    help="output directory")

usecols=['building_id', 'meter', 
    'month_datetime', 'weekofyear_datetime', 
    'dayofyear_datetime', 'hour_datetime', 'day_week', 
    'day_month_datetime', 
    'radiation_intensity_ratio',
    'hour_angle', 'site_id', 'primary_use', 
    'square_feet', 'year_built', 
    'sa_building_side', 'total_sa_building_side', 'est_sa_building', 
    'age', 'air_temperature',
    'dew_temperature', 'precip_depth_1_hr', 'sea_level_pressure',
    'absolute_temperature_diff_pow4', 'offset']

segment_size = 100000
chunksize = segment_size

def train_input_fn(batch_size=4):
    #1. Convert dataframe into correct (features,label) format for Estimator API
    #dataset = tf.data.Dataset.from_tensor_slices(tensors=(dict(df[FEATURE_NAMES]), df[LABEL_NAME]))
    #train_gen = generator(train_size=FLAGS.train_size)

    #sub_df = pd.read_csv("./input/ashrae-energy-prediction/sample_submission.csv")
    #submission_size = max(sub_df.shape)
    #del sub_df
    submission_size = 20216100

    train_gen = read_merged_dataframes(train=False, chunksize=chunksize, streaming=True, skipfooter=submission_size-FLAGS.train_size*segment_size, single_gen=True)#footer?
    dataset = tf.data.Dataset.from_generator(train_gen, output_types=tf.float32, output_shapes=(chunksize, len(usecols)))
    # Note:
    # If we returned now, the Dataset would iterate over the data once  
    # in a fixed order, and only produce a single element at a time.
    
    #2. Shuffle, repeat, and batch the examples.
    dataset = dataset.shuffle(1000).repeat().batch(batch_size)
   
    return dataset

def eval_input_fn(batch_size=4):
    #1. Convert dataframe into correct (features,label) format for Estimator API
    #dataset = tf.data.Dataset.from_tensor_slices(tensors=(dict(df[FEATURE_NAMES]), df[LABEL_NAME]))
    #valid_gen = generator(train_size=FLAGS.train_size)
    
    valid_gen = read_merged_dataframes(train=False, chunksize=chunksize, streaming=True, usecols=usecols, skiprows=FLAGS.train_size*segment_size, single_gen=True)#skiprows?
    dataset = tf.data.Dataset.from_generator(valid_gen, output_types=tf.float32, output_shapes=(chunksize, len(usecols)))
    #2.Batch the examples.
    dataset = dataset.batch(batch_size)
   
    return dataset

def predict_input_fn(batch_size=4):
    #may want to rewrite this?
    #1. Convert dataframe into correct (features) format for Estimator API
    seg_gen = read_merged_dataframes(train=True, streaming=True, usecols=usecols)
    dataset = tf.data.Dataset.from_generator(seg_gen, output_types=tf.float32, output_shapes=(chunksize, len(usecols)))
    #2.Batch the examples.
    dataset = dataset.batch(batch_size)
   
    return dataset

def model_fn(features, labels, mode, params):
    if isinstance(features, dict):  # For serving
        features = features['feature']
    predictions = tf.layers.dense(features, 1)
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=predictions)
    else:
        loss = tf.nn.l2_loss(predictions - labels)
        if mode == tf.estimator.ModeKeys.EVAL:
            return tf.estimator.EstimatorSpec(
                mode, loss=loss)
        elif mode == tf.estimator.ModeKeys.TRAIN:
            train_op = tf.train.AdamOptimizer(learning_rate=0.5).minimize(
                loss, global_step=tf.train.get_global_step())
            return tf.estimator.EstimatorSpec(
                mode, loss=loss, train_op=train_op)
        else:
            raise NotImplementedError()




def train_model():
    feature_columns = ['building_id', 'meter', 
    'month_datetime', 'weekofyear_datetime', 
    'dayofyear_datetime', 'hour_datetime', 'day_week', 
    'day_month_datetime', 
    'radiation_intensity_ratio',
    'hour_angle', 'site_id', 'primary_use', 
    'square_feet', 'year_built', 
    'sa_building_side', 'total_sa_building_side', 'est_sa_building', 
    'age', 'air_temperature',
    'dew_temperature', 'precip_depth_1_hr', 'sea_level_pressure',
    'absolute_temperature_diff_pow4', 'offset']

    shutil.rmtree(FLAGS.outdir, ignore_errors = True) 


    model = tf.estimator.DNNRegressor(
        hidden_units = [[FLAGS.cellsize]*FLAGS.n_layers], # specify neural architecture
        feature_columns = feature_columns, 
        model_dir = FLAGS.outdir,
        config = tf.estimator.RunConfig(tf_random_seed=1)
    )

    model.train(
        input_fn=train_input_fn, 
        steps=500)

    model.save(FLAGS.outdir)

    print_rmse(model)

    estimator = tf.estimator.Estimator(model_fn, 'model', params={})
    estimator.export_saved_model(FLAGS.outdir, serving_input_receiver_fn)

    return

def serving_input_receiver_fn():
    """Serving input_fn that builds features from placeholders

    Returns
    -------
    tf.estimator.export.ServingInputReceiver
    """
    number = tf.placeholder(dtype=tf.float32, shape=[None, 8], name='number')
    receiver_tensors = {'number': number}
    features = tf.tile(number, multiples=[1, 2])
    return tf.estimator.export.ServingInputReceiver(features, receiver_tensors)


def submit_model():
    export_dir = FLAGS.outdir
    subdirs = [x for x in Path(export_dir).iterdir()
           if x.is_dir() and 'temp' not in str(x)]
    latest = str(sorted(subdirs)[-1])
    predict_fn = tf.contrib.predictor.from_saved_model(latest)

    test_feature_generator = read_merged_dataframes(streaming=True, use_cols=use_cols, train=False)

    # Load submission file
    submission = pd.read_csv('input/ashrae-energy-prediction/ashrae_sample_submission.csv', index_col='row_id', dtype={"meter_reading": np.float32})

    for i, features in enumerate(test_feature_generator):
        submission.meter_reading[i] = prediction_fn({'number':features})['output']

    submission.to_csv('results/submission.csv', header=True)



def print_rmse(model):
  metrics = model.evaluate(input_fn=lambda:eval_input_fn())
  print('RMSE on dataset = {}'.format(metrics['average_loss']**.5))

def main(argv):
    train_model()

if __name__ == "__main__":
    tf.logging.set_verbosity(tf.logging.INFO)
    app.run(main)
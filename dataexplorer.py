import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import subprocess
from multiprocessing import Pool
import os

#train_or_test = "train"
#building_metadata_df = pd.read_csv("./input/ashrae-energy-prediction/building_metadata.csv")
#print(building_metadata_df.isna().sum())
#del building_metadata_df
#df = pd.read_csv("./input/ashrae-energy-prediction/%s.csv"%train_or_test)
#print(df.isna().sum())
#del df
#weather_df = pd.read_csv("./input/ashrae-energy-prediction/weather_%s.csv"%train_or_test)
#print(weather_df.isna().sum())
#del weather_df

#df = pd.read_csv("./input/ashrae-energy-prediction/train.csv")
#print(df.shape)
#stc_df =pd.read_csv("site_to_city_mappings.csv")
#print(stc_df.head())

def feature_engineered_train_df_shape():
    return subprocess.check_output('cat ./input/feature-engineered-data/feature_engineered_train.csv | wc -l'.split())

def print_pid(derp):
    print(os.getpid())

def show_shape(path):
    df = pd.read_csv(path)
    print(f'table shape for {path}: {df.shape}')

def show_num_rows(path):
    with open(path) as f:
        for i, l in enumerate(f):
            pass
    print(f"{path} has num rows: {i+1}")
    return

def show_na_rows(path):
    df = pd.read_csv(path)
    print(f'total count of nan values in {path}:\n{df.isna().sum()}')

def show_unique_counts(path):
    df = pd.read_csv(path)
    print(f'displaying unique value counts for {path}')
    for col in df.columns.values:
        print(f'{col}:{df[col].unique().size}')


def read_show_rows(path):
    df = pd.read_csv(path)
    print(f'shape for {path} is {df.shape}')

def show_timestamp_aggs():
    train_df = pd.read_csv('input/ashrae-energy-prediction/train.csv')
    test_df = pd.read_csv('input/ashrae-energy-prediction/test.csv')
    train_df_timestamp = pd.to_datetime(train_df.timestamp) 
    test_df_timestamp = pd.to_datetime(test_df.timestamp) 
    print(f'train_df description: \n {train_df_timestamp.describe()}')
    print(f'test_df description: \n {test_df_timestamp.describe()}')

def explore_directory(files=None, read_directory = 'input/ashrae-energy-prediction', fn=read_show_rows):
    file_list = os.listdir(read_directory)
    if files:
        file_list=[]
        for f in files:
            file_list.append(os.path.join(read_directory, f))
    for file_name in file_list:
        fn(os.path.join(read_directory, file_name))
#read_show_rows('input/ashrae-energy-prediction/train.csv')
#show_timestamp_aggs()
#explore_directory(read_directory='input/ashrae-energy-prediction', fn=show_unique_counts)
#explore_directory(read_directory='input/ashrae-energy-prediction', fn=show_na_rows)
#explore_directory(read_directory='input/feature-engineered-data', fn=show_shape)
#show_na_rows('input/feature-engineered-data/site_city_merged_train.csv')
#explore_directory(read_directory='input/ashrae-energy-prediction', fn=show_num_rows)
#explore_directory(read_directory='input/feature-engineered-data', fn=show_num_rows)
#explore_directory(read_directory="results", fn=read_show_rows)

df = pd.read_csv("results/submission_formatted_prime.csv")
for i, row_id in enumerate(df["row_id"]):
    if i!=row_id:
        print("row_id not consecutive.. @%d"%row_id)
        break
X, y = df["row_id"].astype(np.float32), df["meter_reading"].astype(np.float32)
plt.subplots(figsize=(8, 11))
plt.plot(X, y)
plt.savefig("results/submission_dist.png")
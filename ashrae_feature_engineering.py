import pandas as pd
from pandas.tseries.frequencies import to_offset
import numpy as np
import matplotlib.pyplot as plt
import os
from multiprocessing import Pool, Process, Lock

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import mean_squared_error, accuracy_score, confusion_matrix
import lightgbm as lgb
from sklearn.model_selection import train_test_split

from ashrae_eda_utility import timestamp_align, add_working_day_features, interpolate_by_mean, beaufortize
from gentle_intro_utility import reduce_mem_usage, convert_timestamp, convert_strings_to_category, split_timestamp
from local_better_cities_utility import map_site_to_city
import seaborn as sns

write_directory = "./input/feature-engineered-data"
site_city_write_path = write_directory+'/site_to_city_mappings.csv'
write_chunksize = 50000
merge_df_columns = ",Unnamed: 0_x,building_id,meter,timestamp,meter_reading,month_datetime,weekofyear_datetime,dayofyear_datetime,hour_datetime,day_week,day_month_datetime,week_month_datetime,declination_angle,distance_to_sun,radiation_intensity_ratio,Unnamed: 0_y,site_id,primary_use,square_feet,year_built,floor_count,sa_building_side,total_sa_building_side,est_sa_building,age,Unnamed: 0,air_temperature,cloud_coverage,dew_temperature,precip_depth_1_hr,sea_level_pressure,wind_direction,wind_speed,absolute_temperature_diff_pow4,advection_coef,offset"
merge_df_columns = merge_df_columns.split(',')[1:]
#merge_df_columns.pop(merge_df_columns.index('meter_reading'))


lat_long = {
"Jacksonville": (30.336944, -81.661389),
"Eilat": (29.55, 34.95),
"Phoenix": (33.45, -112.066667),
"Philadelphia": (39.952778, -75.163611),
"San Francisco": (37.783333, -122.416667),
"Jerusalem": (31.783333, 35.216667),
"Montreal": (45.508889, -73.561667),
"San Antonio": (29.416667, -98.5),
"Las Vegas": (36.175, -115.136389),
"Tel Aviv District": (32.066667, 34.783333),
"Minneapolis": (44.981944, -93.269167),
"Toronto": (43.741667, -79.373333),
}

'''lat_long = {
    'orlando': (28.512274, -81.40619),
    'heathrow': (51.471092, -0.455046),
    'Phoenix': (33.474250, -112.077456),
    'washington': (38.8973, -77.02894),
    'sanfranitl': (37.62, -122.365),
    'birmingham': (52.452442, -1.743035),
    'ottowa': (45.414524, -75.711136),
    'sanantonio': (29.419, -98.489),
    'saltlake': (40.894524, -111.88771),
    'dublin': (53.3498, -6.2603),
    'Minneapolis': (44.973814, -93.265767),
    'Philadelphia': (39.958187, -75.15964),
    'rochester': (43.161617, -77.60488),
}'''

def parallelize_dataframe(df, func, num_partitions=10, num_cores=4):
    df_split = np.array_split(df, num_partitions)
    pool = Pool(num_cores)
    df = pd.concat(pool.map(func, df_split))
    pool.close()
    pool.join()
    return df

#def merge_with_reader(args):
def merge_with_reader(read_path, join_columns, merged_df_write_path, is_weather, train):
    if os.path.isfile(merged_df_write_path):
        return
    
    print(f'starting reading and merging for pid: {os.getpid()}')
    #l, reader_params, join_columns, merged_df_write_path, is_weather, train = args
    #join_columns, merged_df_write_path, is_weather, train = args
    reader = pd.read_csv(read_path, chunksize = 50000)#, *reader_params)
    #not allowed to call subprocesses...
    is_train = "train" if train else "test"
    building_metadata_df_write_path = os.path.join(write_directory, 'feature_engineered_building_metadata.csv')
    weather_df_write_path = os.path.join(write_directory,'feature_engineered_weather_%s.csv'%is_train)
    read_path_joinee = weather_df_write_path if is_weather else building_metadata_df_write_path
    joinee_df = pd.read_csv(read_path_joinee)
    #joinee_df = building_metadata_df
    #if is_weather:
    #    joinee_df = weather_df
    try:
        for i, streamed_chunk in enumerate(reader):
            if i%50 == 0:
                print(f"merging {read_path} and {read_path_joinee} dataframes for chunk {i}.. pid: {os.getpid()}")
                #print(streamed_chunk.head())
            header = False
            if not os.path.isfile(merged_df_write_path):
                header=True
            out_df = pd.merge(streamed_chunk, joinee_df, on=join_columns, how='left')
            #l.acquire()
            out_df.to_csv(merged_df_write_path, mode="a", header=header, index=False)
            #l.release()
    except KeyError:
        import pdb; pdb.set_trace()

def parallelize_dataframe_merge(reader_params, join_columns, merged_df_write_path, is_weather, train=True, num_partitions=10, num_cores=4):
    #df_split = np.array_split(df, num_partitions)
    #building_metadata_df_split = [building_metadata_df for _ in range(num_partitions)]
    #weather_df_split = [weather_df for _ in range(num_partitions)]
    

    num_readers = len(reader_params)
    join_columns_ = join_columns*num_readers
    merged_df_write_path_ = [merged_df_write_path]*num_readers
    is_weather_ = [is_weather]*num_readers

    lock = Lock()
    lock_references = [lock]*num_readers
    #pool = Pool(num_cores)
    nargs = zip(lock_references, reader_params, join_columns_, merged_df_write_path_, is_weather_)
    #df = pd.concat(pool.starmap(merge_with_reader, nargs))
    p = Process(target=merge_with_reader, args=nargs)
    #df = pd.concat(pool.map(func, readers))
    #pool.close()
    #pool.join()
    p.start()
    p.join()

def feature_engineered_building_metadata_df(new_features=False):
    write_path = write_directory+'/feature_engineered_building_metadata.csv'
    if os.path.isfile(write_path):
        return pd.read_csv(write_path)
    print("feature engineering for building_metadata...")
    building_metadata_df = pd.read_csv("./input/ashrae-energy-prediction/building_metadata.csv")
    building_metadata_df = reduce_mem_usage(building_metadata_df)

    interpolate_by_mean(building_metadata_df)


    building_metadata_df = parallelize_dataframe(building_metadata_df, feature_engineered_building_metadata_df_helper)
    building_metadata_df.to_csv(write_path, chunksize=write_chunksize)
    return building_metadata_df

def feature_engineered_building_metadata_df_helper(building_metadata_df):
    le = LabelEncoder()
    building_metadata_df['primary_use'] = le.fit_transform(building_metadata_df['primary_use'])
    building_metadata_df['site_id'] = pd.Series(building_metadata_df['site_id'], dtype=int)
    building_metadata_df['primary_use'] = building_metadata_df['primary_use'].astype('category')
    building_metadata_df["sa_building_side"] = np.sqrt(building_metadata_df.square_feet)*building_metadata_df.floor_count
    building_metadata_df["total_sa_building_side"] = 4*building_metadata_df.sa_building_side #assuming most buildings are rectangular prisms
    building_metadata_df["est_sa_building"] = building_metadata_df.total_sa_building_side + building_metadata_df.square_feet
    building_metadata_df['age'] = building_metadata_df['year_built'].max() - building_metadata_df['year_built'] + 1

    return building_metadata_df

def feature_engineered_df(train=True, streaming=False):
    is_train = "train" if train else "test"
    write_path = write_directory+'/feature_engineered_%s.csv'%is_train
    if os.path.isfile(write_path):
        if streaming:
            return pd.read_csv(write_path, chunksize=write_chunksize)
        return pd.read_csv(write_path)
    print("feature engineering for %s..."%is_train)

    if streaming:
        reader = pd.read_csv("./input/ashrae-energy-prediction/%s.csv"%is_train, chunksize=write_chunksize)
        for i, chunk in enumerate(reader):
            if i%200 == 0:
                print("feature engineering for chunk %s chunksize: %s"%(i, write_chunksize))
            chunk = feature_engineered_df_helper(chunk)
            header = False
            if not os.path.isfile(write_path):
                header = True
            chunk.to_csv(write_path, mode='a', header=header)
        return pd.read_csv(write_path, chunksize=write_chunksize)


    df = pd.read_csv("./input/ashrae-energy-prediction/%s.csv"%is_train)
    df = reduce_mem_usage(df)

    split_timestamp(df)
    ante_date = np.datetime64('2016-05-20 00:00:00')
    if train:
        df = df.where(~((df.building_id <= 104) & (df.meter == 0) & (df.timestamp <= ante_date))).dropna(subset=['timestamp'])
    #df = feature_engineered_df_helper(df)
    #cannot do this since we are using interpolate_by_mean?
    #its fine local/rolling mean as long as window size happens to be ok?
    df = parallelize_dataframe(df, feature_engineered_df_helper)

    #or use a shared memory space to avg
    print("writing df main input")

    df.to_csv(write_path)
    return df

def feature_engineered_df_helper(df):
    #change next for test data?
    #will the following line work for test data?-probably screws up submission dimensions-this was also cped from a notebook

    interpolate_by_mean(df)
    df["declination_angle"] = 360*(df.dayofyear_datetime-81)/365
    df["declination_angle"] = np.radians(23.45)*np.sin(df["declination_angle"])
    df["distance_to_sun"] = 360*(df.dayofyear_datetime-93)/365#Mkm
    df["distance_to_sun"] = 150*(1+0.017*np.sin(df["distance_to_sun"]))
    radius_of_sun = 6.955*10**2#Mkm
    df["radiation_intensity_ratio"] = (radius_of_sun/df.distance_to_sun)**2
    #new features 11/5/19
    B = 360/364*(df.dayofyear_datetime - 81)
    E = 9.87*np.sin(2*B) - 7.53*np.cos(B) - 1.5*np.sin(B)
    df["E"] = pd.Series(E).round()
    df["E"] = pd.to_timedelta(df.E, unit='m')
    df["solar_time"] = pd.to_datetime(df.timestamp)
    df["solar_time"] = df.solar_time+df.E
    df.drop(columns=["E"])
    df["hour_angle"] = np.radians(15)*(12-df.solar_time.dt.hour.astype(np.int8))
    return df

def feature_engineered_weather_df(train=True):
    is_train = "train" if train else "test"
    write_path = write_directory+'/feature_engineered_weather_%s.csv'%is_train
    if os.path.isfile(write_path):
        return pd.read_csv(write_path)

    print("feature engineering for weather_%s"%is_train)
    weather_df = pd.read_csv("./input/ashrae-energy-prediction/weather_%s.csv"%is_train)
    weather_df = reduce_mem_usage(weather_df)

    weather_df = weather_df.groupby('site_id').apply(lambda group: group.interpolate(limit_direction='both'))
    interpolate_by_mean(weather_df)
    beaufortize(weather_df)

    weather_df['site_id'] = pd.Series(weather_df['site_id'], dtype=int)

    weather_df = weather_df.groupby('site_id').apply(lambda group: group.interpolate(limit_direction='both'))

    weather_df['timestamp'] = pd.to_datetime(weather_df['timestamp'])
    weather_df["absolute_temperature_diff_pow4"] = (weather_df.dew_temperature + 274.15) - (weather_df.air_temperature + 274.15)
    weather_df["advection_coef"] = weather_df.wind_speed*weather_df.sea_level_pressure

    weather_df = timestamp_align(weather_df)
    
    weather_df = reduce_mem_usage(weather_df)#?
    
    weather_df.to_csv(write_path, chunksize=write_chunksize)
    return weather_df

def feature_engineered_building_merged_df(df):

    site_city_df = pd.read_csv(site_city_write_path)

    df["site_id"] = pd.Series(df.site_id, dtype=np.int64)
    print(site_city_df.site_id)
    #int(k.split('_')[1])
    site_to_city_map = {k:v for k, v in zip(site_city_df.site_id, site_city_df.city)}
    df["city"] = df.site_id.apply(lambda x: site_to_city_map[x])
    print(df.columns.values)
    print(df.head())
    df = df.merge(site_city_df, on=["site_id"], how="left")

    interpolate_by_mean(df)
    #df = df.dropna(subset=["site_id", "city"])
    try:
        df["latitude_angle"] = df["city"].apply(lambda x: lat_long[x][0])
    except KeyError as e:
        print('WTF KEYERROR')
        print(df.city.unique())
        print(e)
        print(df.isna().sum())
        #df = df.dropna()

    try:
        df["sun_altitude_angle"] = np.arccos(np.sin(df.declination_angle)*np.sin(df.latitude_angle) + np.cos(df.declination_angle)*np.cos(df.latitude_angle)*np.cos(df.hour_angle))
    except AttributeError as e:
        print(e)
        print(df.columns.values)
    df["sun_azimuth_angle1"] = np.arcsin(np.cos(df.declination_angle)*np.sin(df.hour_angle)/np.cos(df.sun_altitude_angle))
    df["sun_azimuth_angle2"] = np.arccos((np.sin(df.sun_altitude_angle)*np.sin(df.latitude_angle)-np.sin(df.declination_angle))/(np.cos(df.sun_altitude_angle)*np.cos(df.latitude_angle)))

    df["sun_azimuth_angle1"] = df["sun_azimuth_angle1"].where(df["sun_azimuth_angle1"] <= 90)
    df["sun_azimuth_angle2"] = df["sun_azimuth_angle2"].where(df["sun_azimuth_angle2"] <= 90)
    df["sun_azimuth_angle"] = df["sun_azimuth_angle1"]
    for i, angle in enumerate(df.sun_azimuth_angle):
        if angle == 'nan':#or NaN?
            df["sun_azimuth_angle"][i] = df["sun_azimuth_angle2"][i]
    df.drop(columns=["sun_azimuth_angle1", "sun_azimuth_angle2"])
    
    #print(df.sun_azimuth_angle.isna().sum())
    #probably doesn't work
    #df["sun_azimuth_angle"] = df.sun_azimuth_angle1 if df.sun_azimuth_angle1 <= 90 else df.sun_azimuth_angle2#fix this
    return df

def get_reader_params(read_path, num_rows_df=20216101, num_partitions=10, chunksize=50000):
    partition_size = int(num_rows_df/num_partitions)
    return [{'skiprows':partition*partition_size, 'nrows':partition_size, 'chunksize':chunksize} for partition in range(num_partitions)]

def get_readers(read_path, num_rows_df=20216101, num_partitions=10, chunksize=50000):
    partition_size = int(num_rows_df/num_partitions)
    readers = []
    for partition in range(num_partitions):
        skip_rows = (partition*partition_size)
        readers.append([pd.read_csv(read_path,
            skiprows=skip_rows,
            nrows=partition_size, 
            chunksize=chunksize)])
    return readers

def write_merged_dataframes(train=True, check_dims=False):
    is_train = "train" if train else "test"

    building_metadata_df_path = os.path.join(write_directory, 'feature_engineered_building_metadata.csv')
    df_path = os.path.join(write_directory, 'feature_engineered_%s.csv'%is_train)
    weather_df_path = os.path.join(write_directory, 'feature_engineered_weather_%s.csv'%is_train)

    building_merged_df_path = os.path.join(write_directory, "building_merged_%s.csv"%is_train)
    site_city_merged_df_path = os.path.join(write_directory, "site_city_merged_%s.csv"%is_train)
    merged_df_path = os.path.join(write_directory, "merged_data_%s.csv"%is_train)

    if not os.path.isfile(site_city_write_path):
        map_site_to_city()

    #global building_metadata_df
    #global weather_df
    building_metadata_df = feature_engineered_building_metadata_df()
    #df = 
    feature_engineered_df(train=train)
    weather_df = feature_engineered_weather_df(train=train)

    print("merging tables..")

    merge_with_reader(read_path=df_path,
        join_columns=['building_id'], 
        merged_df_write_path=building_merged_df_path, 
        is_weather=False, 
        train=train)
    '''reader_params = get_reader_params(read_path=df_path, 
        num_rows_df=20216101,
        num_partitions=10, 
        chunksize=50000)

    parallelize_dataframe_merge(reader_params=reader_params, 
        join_columns=['building_id'], 
        merged_df_write_path=building_merged_df_path, 
        is_weather=False, 
        train=train,
        num_partitions=10, 
        num_cores=4)'''
    
    
    reader = pd.read_csv(building_merged_df_path, chunksize=50000)
    #feature_engineered_building_merged_df(df)
    if not os.path.isfile(site_city_merged_df_path):
        for i, chunk in enumerate(reader):
            if i % 50 == 0:
                print(f"mapping site to city for chunk: {i}")
            
            feature_engineered_building_merged_df(chunk)
            #is next line faster at all?- startup times for processes, etc
            #chunk = parallelize_dataframe(chunk, feature_engineered_building_merged_df)
            header=False
            if not os.path.isfile(site_city_merged_df_path):
                header=True
            chunk.to_csv(site_city_merged_df_path, mode='a', header=header)
    #del df
    merge_with_reader(read_path=building_merged_df_path,
        join_columns=['site_id', 'timestamp'], 
        merged_df_write_path=merged_df_path,
        is_weather=True,
        train=train)
    '''reader_params = get_reader_params(read_path=building_merged_df_path, 
        num_rows_df=20216101, 
        num_partitions=10, 
        chunksize=50000)
    
    parallelize_dataframe_merge(reader_params=reader_params, 
        join_columns=['site_id', 'timestamp'], 
        merged_df_write_path=merged_df_path, 
        is_weather=True, 
        train=train,
        num_partitions=10, 
        num_cores=4)'''
    #del building_metadata_df
    #del weather_df
    


    '''df = df.merge(building_metadata_df, on=['building_id'], how='left')
    feature_engineered_building_merged_df(df)#shared resource site_to_city_mappings.csv ?
    df = df.merge(weather_df, on=['site_id', 'timestamp'], how='left')

    plot_save_meter_reading('figures/merged.png', df)
    df.to_csv(merged_df_write_path)'''



def merge_df_helper(df_tuple):
    df, building_metadata_df, weather_df = df_tuple
    if isinstance(df, str):
        print("df is a string instance?????")
    if isinstance(building_metadata_df, str):
        print("building_metadata_df is a string instance?????")
    if isinstance(weather_df, str):
        print("weather_df is a string instance?????")
    df = df.merge(building_metadata_df, on=['building_id'], how='left')
    feature_engineered_building_merged_df(df)#shared resource site_to_city_mappings.csv ?
    df = df.merge(weather_df, on=['site_id', 'timestamp'], how='left')
    return df

def read_merged_dataframes(train=True, chunksize=2000000, streaming=False, usecols=None, skiprows=None, skipfooter=0, single_gen=False):
    is_train = "train" if train else "test"
    read_path = "./input/feature-engineered-data/merged_data_%s.csv"%is_train
    if not os.path.isfile(read_path):
        write_merged_dataframes(train=train)
    
    if streaming:
        columns = merge_df_columns
        if usecols:
            columns = usecols
        #columns.pop(columns.index('meter_reading'))
        if single_gen:
            columns = columns.append('meter_reading')
            print('single generator output')
            if not columns:
                print('usecols parameter still passed in as None?')
            return pd.read_csv(read_path, chunksize=chunksize, usecols=columns, skipfooter=skipfooter, skiprows=skiprows)
        df = pd.read_csv(read_path, chunksize=chunksize)
        X_reader = pd.read_csv(read_path, usecols=columns, chunksize=chunksize)
        y_reader = pd.read_csv(read_path, usecols=['meter_reading'], chunksize=chunksize)
        return X_reader, y_reader
    df = pd.read_csv(read_path)
    #df = reduce_mem_usage(df)
    return df[columns], df['meter_reading']

def plot_save_meter_reading(filename, df):
    filepath = 'figures'
    fig, ax = plt.subplots(figsize=(13, 8))
    ax.plot(df.meter_reading)
    plt.savefig(filepath+filename)

#read_merged_dataframes(train=True, streaming=True)
#read_merged_dataframes(train=False, streaming=True)

def plot_save_correlations(df):
    correlation_matrix = df.corr()
    fig = plt.figure(figsize=(14, 6))
    fig.add_subplot(1, 1, 1)
    ax = sns.heatmap(correlation_matrix, annot=True, cmap='bwr')
    plt.savefig("./figures/feature_correlation_matrix.png")
    plt.show()
    correlation_matrix.to_csv("./results/feature_correlation_matrix.csv")

#
#building_metadata_df_columns: ['site_id' 'building_id' 'primary_use' 'square_feet' 'year_built'
# 'floor_count']
#train_df_columns: ['building_id' 'meter' 'timestamp' 'meter_reading']
#test_df_columns: [row_id,building_id,meter,timestamp]
#weather_train_columns: ['site_id' 'timestamp' 'air_temperature' 'cloud_coverage'
# 'dew_temperature' 'precip_depth_1_hr' 'sea_level_pressure'
# 'wind_direction' 'wind_speed']
# site_id,timestamp,air_temperature,cloud_coverage,
# dew_temperature, precip_depth_1_hr,sea_level_pressure,wind_direction,wind_speed
#
# df left join weather on site_id, timestamp
# -> left join building_metadata_df on site_id, building_id

#['building_id' 'meter' 'month_datetime' 'weekofyear_datetime'
# 'dayofyear_datetime' 'hour_datetime' 'day_week' 'day_month_datetime'
# 'week_month_datetime' 'declination_angle' 'distance_to_sun'
# 'radiation_intensity_ratio' 'site_id' 'primary_use'
# 'square_feet' 'year_built' 'floor_count' 'sa_building_side'
# 'total_sa_building_side' 'est_sa_building' 'age' 
# 'air_temperature' 'cloud_coverage' 'dew_temperature' 'precip_depth_1_hr'
# 'sea_level_pressure' 'wind_direction' 'wind_speed'
# 'absolute_temperature_diff_pow4' 'advection_coef' 'offset']
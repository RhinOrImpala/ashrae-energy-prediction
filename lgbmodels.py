import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import optuna

import os

from sklearn.preprocessing import LabelEncoder
from sklearn import metrics
from math import sqrt

from sklearn.metrics import mean_squared_error
import lightgbm as lgb
from sklearn.model_selection import train_test_split

#from ashrae_feature_engineering import write_merged_dataframes, read_merged_dataframes
#from feature_plucker import examine_wing_feathers
from streaming_fe_merge import read_merged_dataframes

from multiprocessing import Pool

from codetiming import Timer

import pickle

usecols=['building_id', 'meter', 
    'month_datetime', 'weekofyear_datetime', 
    'dayofyear_datetime', 'hour_datetime', 'day_week', 
    'day_month_datetime', 'week_month_datetime', 
    'radiation_intensity_ratio',
    'hour_angle', 'site_id', 'primary_use', 
    'square_feet', 'year_built', 'floor_count', 
    'sa_building_side', 'total_sa_building_side', 'est_sa_building', 
    'age', 'air_temperature', 'cloud_coverage', 
    'dew_temperature', 'precip_depth_1_hr', 'sea_level_pressure', 
    'wind_direction', 'wind_speed', 'beaufort_scale', 
    'absolute_temperature_diff_pow4', 'advection_coef', 'offset']

usecols=['building_id', 'meter', 
    'month_datetime', 'weekofyear_datetime', 
    'dayofyear_datetime', 'hour_datetime', 'day_week', 
    'day_month_datetime', 
    'radiation_intensity_ratio',
    'hour_angle', 'site_id', 'primary_use', 
    'square_feet', 'year_built', 
    'sa_building_side', 'total_sa_building_side', 'est_sa_building', 
    'age', 'air_temperature',
    'dew_temperature', 'precip_depth_1_hr', 'sea_level_pressure',
    'absolute_temperature_diff_pow4', 'offset', 'meter_reading']
valid_np_numeric_types = [np.int, np.int8, np.int16, np.int32, np.int64, np.float, np.float16, np.float32, np.float64]



def extract_transform_dataset(usecols, train=True, meter=-1, logify_labels=True):
    print(f"loading {'train' if train else 'test'} dataset...")
    #reader = read_merged_dataframes(streaming=True, train=True, usecols=usecols, chunksize=5000000, single_gen=True)
    df = read_merged_dataframes(train=train)
    dropcols = [col for col in df.columns.values if col not in usecols]
    
    df = df.drop(columns=dropcols)
    '''for col in df.columns.values:
        if not col.dtype in valid_np_numeric_types:
            print("casting column: %s to numeric type"%col.name)
            col = col.astype(np.float64)'''
    #print(df.columns.values)
    if meter != -1:
        df = df.where(df.meter==meter).dropna(subset=['meter'])
    
    X = df[filter(lambda x: x != 'meter_reading', df.columns.values)]
    if train:
        y = df['meter_reading']
        print(X.columns.values)
        if logify_labels:
            y = np.log1p(y)
        return X, y
    return X

def read_load_results_model(model_path='lgbmResults/lgbm_model.dat', results_path='lgbmResults/lgbm_search_results.pkl'):
    with open(model_path, 'rb') as f:
        best_estimator = pickle.load(f)

    with open(results_path, 'rb') as f:
        result = pickle.load(f)
    return result, best_estimator

def plot_save_featimps(gbm):
    feature_imp = pd.DataFrame(sorted(zip(gbm.feature_importance(), gbm.feature_name()),reverse = True), columns=['Value','Feature'])
    plt.figure(figsize=(10, 5))
    sns.barplot(x="Value", y="Feature", data=feature_imp.sort_values(by="Value", ascending=False))
    plt.title('LightGBM FEATURES')
    plt.tight_layout()
    plt.savefig("./figures/lgb_feature_importance.png")

def objective(trial, usecols, data, target, meter=-1):
    seed = 42#10?
    test_size = .5#0.22
    #print(usecols)
    #data, target = extract_transform_dataset(usecols=usecols, meter=meter)
    #any better way todo this than read and filter every time?
    print("beginning training")
    train_x, test_x, train_y, test_y = train_test_split(data, target, test_size=test_size, random_state=seed)
    dtrain = lgb.Dataset(train_x, label=train_y)
    #print(type(target))
    #print(target.shape)
    #print(target.dtype)
    param = {
        'objective': 'regression',
        'metric': 'rmse',
        'verbosity': 5,
        'boosting_type': 'gbdt',
        'lambda_l1': trial.suggest_loguniform('lambda_l1', 1e-8, 10.0),
        'lambda_l2': trial.suggest_loguniform('lambda_l2', 1e-8, 10.0),
        'num_leaves': trial.suggest_int('num_leaves', 2, 256),
        'feature_fraction': trial.suggest_uniform('feature_fraction', 0.4, 1.0),
        'bagging_fraction': trial.suggest_uniform('bagging_fraction', 0.4, 1.0),
        'bagging_freq': trial.suggest_int('bagging_freq', 1, 7),
        'min_child_samples': trial.suggest_int('min_child_samples', 5, 100),
    }

    gbm = lgb.train(param, dtrain)
    preds = gbm.predict(test_x)
    rmse = sqrt(mean_squared_error(test_y, preds))
    return rmse

def train_model(meter=-1):
    print("beginning parameter optimization for meter: %s"%meter)
    sampler = optuna.samplers.TPESampler(seed=10)
    study = optuna.create_study(sampler=sampler, direction='minimize')
    
    data, target = extract_transform_dataset(usecols=usecols, meter=meter)
    print("optimizing..")
    study.optimize(lambda trial: objective(trial, usecols, data, target, meter), n_trials=40)
    print('Number of finished trials: {}'.format(len(study.trials)))
    print('Best trial:')
    trial = study.best_trial
    print('  Value: {}'.format(trial.value))
    with open('best_scores.txt', 'wb') as f:
        f.write("best score for meter {meter} is: {trial.value}")
    print('  Params: ')
    for key, value in trial.params.items():
        print('    {}: {}'.format(key, value))

    best_params = pd.DataFrame(trial.params.items())
    best_params.to_csv(f'results/best_params_meter_{meter}.csv')

def read_best_params_train_model(meter, data, target):
    param_df = pd.read_csv(f'results/best_params_meter_{meter}.csv')
    keys, values = param_df['0'], param_df['1']
    params = {k:v for k, v in zip(keys, values)}

    #do this on write best_params?-later
    params_to_int = ['num_leaves', 'bagging_freq', 'min_child_samples']
    for param in params_to_int:
        params[param] = int(params[param])

    seed = 42#10?
    test_size = 0.22
    #print(usecols)
    print('loading train dataset')
    #any better way todo this than read and filter every time?
    print(f'data.columns.values: {data.columns.values}')
    print("beginning training")
    train_x, test_x, train_y, test_y = train_test_split(data, target, test_size=test_size, random_state=seed)
    dtrain = lgb.Dataset(train_x, label=train_y)

    gbm = lgb.train(params, dtrain)
    preds = gbm.predict(test_x)
    rmse = sqrt(mean_squared_error(test_y, preds))
    print(f'trained model with meter: {meter} rmse: {rmse}')
    return gbm

    #with open('{}_meter{}.pickle'.format(study.best_trial.number, meter), 'rb') as fin:
    #    best_clf = pickle.load(fin)

def get_best_params_per_meter():
    for meter in range(4):
        train_model(meter)

def parallelize_dataframe(df, func, num_partitions=10, num_cores=4):
    df_split = np.array_split(df, num_partitions)
    pool = Pool(num_cores)
    df = pd.concat(pool.map(func, df_split))
    pool.close()
    pool.join()
    return df

'''def predict_with_model(models, row):
    model = models[row['meter']]
    #are indicies preserved?
    return model.predict(row)'''

def predict_with_model(models):
    def predict_row(row):
        model = models[row['meter']]
        return model.predict(row)
    #are indicies preserved?
    return predict_row

#better way to do this? causes alot of 
def predict(models, df):
    
    preds = df.apply(predict_with_model(models), axis=1)
    meter_readings = pd.Series(preds, name='meter_reading')
    meter_df = pd.DataFrame([meter_readings])
    meter_df = meter_df.reset_index()
    meter_df = meter_df.rename({"index":"row_id"})

    return meter_df

chunksize=100000

def write_submit_streaming(usecols, write_path = 'results/submission.csv'):
    models = {}
    data, target = extract_transform_dataset(usecols=usecols, train=True, meter=-1, logify_labels=False)
    for meter in range(4):
        model_write_path = f"models/lgbmodel{meter}.txt"
        
        if not os.path.isfile(model_write_path):
            print(f'training model on best params for meter: {meter}')
            models[meter] = read_best_params_train_model(meter, data, target)
            models[meter].save_model(model_write_path)
        else:
            models[meter] = lgb.Booster(model_file=model_write_path)

    merged_test_path = "input/feature-engineered-data/merged_data_test.csv"
    
    for i, chunk in enumerate(pd.read_csv(merged_test_path, chunksize=chunksize)):
        if i%5 == 0:
            print(f"predicting and writing on rowchunk {i*chunksize}")
        dropcols = [col for col in chunk.columns.values if col not in usecols]
        chunk = chunk.drop(columns=dropcols)
        preds = predict(models, chunk)
        preds = preds.transpose()
        header = not os.path.isfile(write_path)
        preds.to_csv(write_path, mode='a', header=header, index=False)

def format_submission(read_path = 'results/submission.csv', write_path='results/submission_formatted.csv'):
    if not os.path.isfile(write_path):
        for i, chunk in enumerate(pd.read_csv(read_path, chunksize=chunksize)):
            if i%5 == 0:
                print(f"formatting and writing on rowchunk {i*chunksize}")
            chunk = chunk['0']

            chunk = chunk.apply(lambda x: x[1:-1])
            chunk = chunk.where(chunk != 'eter_readin')
            chunk = chunk.dropna()

            #chunk['row_id'] = chunk['row_id'].astype(np.int32) - (1+i)
            header = not os.path.isfile(write_path)
            chunk.to_csv(write_path, mode='a', header=header, index=False)

    end_write_path = "results/submission_formatted_prime.csv"

    df = pd.read_csv(write_path)
    #os.remove(write_path)
    df = df.reset_index()
    try:
        assert(len(df.columns.values) == 2 and isinstance(df, pd.DataFrame))
    except AssertionError:
        import pdb; pdb.set_trace()
    df = df.rename(columns={k:v for k, v in zip(df.columns.values, ['row_id', 'meter_reading'])})
    df['row_id'] = df['row_id'].astype(np.int32)
    df.to_csv(end_write_path, index=False)#don't need a os.rm

#get_best_params_per_meter()
with Timer(name='write submission'):
    format_submission()
    #write_submit_streaming(usecols=usecols)

lgb_params = {
            'boosting_type': 'gbdt',
            'objective': 'regression',
            'metric': {'rmse'},
            'subsample': 0.4,
            'subsample_freq': 1,
            'learning_rate': 0.25,
            'num_leaves': 31,
            'feature_fraction': 0.8,
            'lambda_l1': 1,
            'lambda_l2': 1
            }


def plot_data(df, columns):
    for column in columns:
        plt.subplots(figsize=(13, 8))
        plt.plot(df[column])
        plt.savefig(f"figures/{column}_dataviz.png")
        plt.cla()
        plt.clf()
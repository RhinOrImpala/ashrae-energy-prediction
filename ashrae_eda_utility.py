
import pandas as pd
from pandas.tseries.holiday import USFederalHolidayCalendar as calendar
import numpy as np
import os
import math

write_directory = "./input/feature-engineered-data"
write_chunksize = 50000

#https://www.kaggle.com/kaushal2896/ashrae-eda-fe-lightgbm-1-13
def get_site_ids_offsets():
    site_id_path = write_directory+"/site_ids_offsets.csv"
    if os.path.isfile(site_id_path):
        return pd.read_csv(site_id_path)
    weather_train_df = pd.read_csv("input/ashrae-energy-prediction/weather_train.csv")
    weather_test_df = pd.read_csv("input/ashrae-energy-prediction/weather_test.csv")
    weather = pd.concat([weather_train_df,weather_test_df],ignore_index=True)
    weather_key = ['site_id', 'timestamp']

    temp_skeleton = weather[weather_key + ['air_temperature']].drop_duplicates(subset=weather_key).sort_values(by=weather_key).copy()

    # calculate ranks of hourly temperatures within date/site_id chunks
    temp_skeleton.timestamp = pd.to_datetime(temp_skeleton.timestamp)

    temp_skeleton['temp_rank'] = temp_skeleton.groupby(['site_id', temp_skeleton.timestamp.dt.date])['air_temperature'].rank('average')

    # create a dataframe of site_ids (0-16) x mean hour rank of temperature within day (0-23)
    df_2d = temp_skeleton.groupby(['site_id', temp_skeleton.timestamp.dt.hour])['temp_rank'].mean().unstack(level=1)

    # Subtract the columnID of temperature peak by 14, getting the timestamp alignment gap.
    site_ids_offsets = pd.Series(df_2d.values.argmax(axis=1) - 14)
    site_ids_offsets.index.name = 'site_id'
    site_ids_offsets.to_csv(site_id_path)
    return site_ids_offsets

def timestamp_align(df):
    site_ids_offsets = get_site_ids_offsets()
    if isinstance(site_ids_offsets, pd.DataFrame):
        print("converted from dataframe to series")
        column = site_ids_offsets.columns.values[0]
        site_ids_offsets = site_ids_offsets[column]
    #if isinstance(site_ids_offsets, pd.DataFrame):
    df['offset'] = df.site_id.map(site_ids_offsets)
    df['timestamp_aligned'] = (df.timestamp - pd.to_timedelta(df.offset, unit='H'))
    df['timestamp'] = df['timestamp_aligned']
    del df['timestamp_aligned']
    return df

def add_working_day_features(df):
    # Assuming 5 days a week for all the given buildings
    df.loc[(df['weekday'] == 5) | (df['weekday'] == 6) , 'is_holiday'] = 1

    dates_range = pd.date_range(start='2015-12-31', end='2019-01-01')
    us_holidays = calendar().holidays(start=dates_range.min(), end=dates_range.max())
    df['is_holiday'] = (df['timestamp'].dt.date.astype('datetime64').isin(us_holidays)).astype(np.int8)

def mean_without_overflow_fast(col):
    col /= len(col)
    return col.mean() * len(col)

def interpolate_by_mean(df):
    missing_values = (100-df.count() / len(df) * 100).sort_values(ascending=False)
    missing_features = df.loc[:, missing_values > 0.0]
    missing_features = missing_features.apply(mean_without_overflow_fast)

    for key in df.loc[:, missing_values > 0.0].keys():
        if key == 'year_built' or key == 'floor_count':
            df[key].fillna(math.floor(missing_features[key]), inplace=True)
        else:
            df[key].fillna(missing_features[key], inplace=True)

def beaufortize(weather_df):
    beaufort = [(0, 0, 0.3), (1, 0.3, 1.6), (2, 1.6, 3.4), (3, 3.4, 5.5), (4, 5.5, 8), (5, 8, 10.8), (6, 10.8, 13.9), 
          (7, 13.9, 17.2), (8, 17.2, 20.8), (9, 20.8, 24.5), (10, 24.5, 28.5), (11, 28.5, 33), (12, 33, 200)]
    for item in beaufort:
        weather_df.loc[(weather_df['wind_speed']>=item[1]) & (weather_df['wind_speed']<item[2]), 'beaufort_scale'] = item[0]
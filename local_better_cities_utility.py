import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os

#https://www.kaggle.com/datadugong/locate-better-cities-by-weather-temp-fill-nans

def map_site_to_city():
    write_path = 'input/feature-engineered-data/site_to_city_mappings.csv'
    if not os.path.isfile(write_path):
        # merge weather_train & weather_test
        print("generating site to city mappings")
        weather_train = pd.read_csv('./input/ashrae-energy-prediction/weather_train.csv')
        weather_test = pd.read_csv('./input/ashrae-energy-prediction/weather_test.csv')
        weather_merged = weather_train.append(weather_test)
        weather_merged['timestamp'] = pd.to_datetime(weather_merged['timestamp'])
        weather_merged.set_index('timestamp', inplace=True)

        #extract temperature from weather data
        df_temperature_pivot = weather_merged.reset_index().pivot_table(index='timestamp', columns='site_id', values='air_temperature')
        #df_temperature_pivot.columns = 'site_'+df_temperature_pivot.columns.astype('str')
        df_temperature_pivot.columns = df_temperature_pivot.columns.astype('str')

        #load external temperature data
        temperature_external = pd.read_csv("./input/historical-hourly-weather-data/temperature.csv")
        temperature_external['datetime'] = pd.to_datetime(temperature_external['datetime'])
        temperature_external.set_index('datetime', inplace=True)
        temperature_external = temperature_external-273.15
        temperature_external = temperature_external.merge(df_temperature_pivot, left_index=True, right_index=True, how='inner')
        temperature_external = temperature_external.dropna()

        #calculate correlations between sites
        df_corr = temperature_external.corr(method='spearman')
        list_site = df_temperature_pivot.columns
        df_corr = df_corr[list_site]
        df_corr = df_corr.drop(list_site)

        #Get cities!
        df_findCity = pd.concat([df_corr.idxmax(),df_corr.max()], axis=1).reset_index().rename(columns={'index':'site_id',0:'city',1:'corr'})
        df_findCity.to_csv(write_path)